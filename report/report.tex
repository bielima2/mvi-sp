%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Journal Article
% LaTeX Template
% Version 1.4 (15/5/16)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% Original author:
% Frits Wenneker (http://www.howtotex.com) with extensive modifications by
% Vel (vel@LaTeXTemplates.com)
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% -------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
% -------------------------------------------------------------------------------

\documentclass[twoside,twocolumn]{article}

\usepackage[sc]{mathpazo} % Use the Palatino font
\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\linespread{1.05} % Line spacing - Palatino needs more space between lines
\usepackage{microtype} % Slightly tweak font spacing for aesthetics

\usepackage[english]{babel} % Language hyphenation and typographical rules

\usepackage[hmarginratio=1:1,top=32mm,columnsep=20pt]{geometry} % Document
% margins
\usepackage[hang, small,labelfont=bf,up,textfont=it,up]{caption} % Custom
                                % captions under/above floats in tables or
                                % figures
\usepackage{booktabs} % Horizontal rules in tables

\usepackage{lettrine} % The lettrine is the first enlarged letter at the
% beginning of the text

\usepackage{enumitem} % Customized lists
\setlist[itemize]{noitemsep} % Make itemize lists more compact

% \usepackage{abstract} % Allows abstract customization
% Set the "Abstract" text to bold
% \renewcommand{\abstractnamefont}{\normalfont\bfseries}
% \renewcommand{\abstracttextfont}{\normalfont\small\itshape} % Set the abstract
% itself to small italic text

\usepackage{titlesec} % Allows customization of titles
\renewcommand\thesection{\Roman{section}} % Roman numerals for the sections
\renewcommand\thesubsection{\roman{subsection}} % roman numerals for subsections
% Change the look of the section titles
\titleformat{\section}[block]{\large\scshape\centering}{\thesection.}{1em}{}
\titleformat{\subsection}[block]{\large}{\thesubsection.}{1em}{} % Change the
% look of the section titles

% \usepackage{fancyhdr} % Headers and footers
% % \pagestyle{fancy} % All pages have headers and footers
% \fancyhead{} % Blank out the default header
% \fancyfoot{} % Blank out the default footer
% \fancyhead[C]{Running title $\bullet$ May 2016 $\bullet$ Vol. XXI, No. 1} % Custom header
% % text
% \fancyfoot[RO,LE]{\thepage} % Custom footer text

\usepackage{titling} % Customizing the title section

\usepackage{hyperref} 
\usepackage{mathtools}
\usepackage{graphicx}
\graphicspath{ {./fig/} }

% -------------------------------------------------------------------------------
%	TITLE SECTION
% -------------------------------------------------------------------------------

\setlength{\droptitle}{-4\baselineskip} % Move the title up

\pretitle{\begin{center}\Huge\bfseries} % Article title formatting
  \posttitle{\end{center}} % Article title closing formatting
\title{Malware detection utilizing artificial neural networks} % Article title
\author{%
  \textsc{Marek Bielik} \\[1ex] % Your name
  \normalsize Czech Technical University in Prague\\ % Your institution
  \normalsize \href{mailto:bielima2@fit.cvut.cz}{bielima2@fit.cvut.cz} 
  % \and % Uncomment if 2 authors are required, duplicate these 4 lines if more
  % \textsc{Jane Smith}\thanks{Corresponding author} \\[1ex] % Second author's
  % name
  % \normalsize University of Utah \\ % Second author's institution
  % \normalsize \href{mailto:jane@smith.com}{jane@smith.com} % Second author's
  % email address
}
\date{\today} % Leave empty to omit a date
\renewcommand{\maketitlehookd}{%
  % \begin{abstract}
  %   \noindent \blindtext % Dummy abstract text - replace \blindtext with your
  %   % abstract text
  % \end{abstract}
}

% -------------------------------------------------------------------------------

\begin{document}

\maketitle

% -------------------------------------------------------------------------------
% ARTICLE CONTENTS
% -------------------------------------------------------------------------------

\section{Introduction}

Artificial neural networks such as MLP (Multilayer Perceptron) and CNN
(Convolutional Neural Network) have been successfully used to statically
classify malware \cite{m1, m2, m3}. We will also focus on these networks and try
to explore their capabilities.

% ---------------------------------------------

\section{Data Set}

The EMBER data set \footnote{\url{https://github.com/endgameinc/ember}}
\cite{2018arXiv180404637A} contains features extracted from executable files in
the PE (Portable Executable) format. Each file is abstracted into a JSON object
which can be subsequently transformed into a feature vector containing 2351
dimensions. Besides the JSON data, the authors of the data set also provided
methods for transforming it into the feature vectors. These methods implement
embedding techniques, such as the hashing trick where appropriate. We will focus
on EMBER2018 which contains:

\begin{itemize}
\item{training data:}
  \begin{itemize}
  \item{200 000 unlabelled samples,}
  \item{300 000 malicious samples,}
  \item{300 000 benign samples,}
  \end{itemize}
\item{test data:}
  \begin{itemize}
  \item{100 000 malicious samples,}
  \item{100 000 benign samples.}
  \end{itemize}
\end{itemize}

% ---------------------------------------------

\section{Data Preprocessing}

At the very beginning, the unlabelled samples were cleared away. The first
obstacle that emerged during the process of building our classifiers was that it
was not possible to train it on the raw 2351 dimensional vectors provided by the
data set. The accuracy even on the training data was always no better than the
accuracy of a random classifier regardless of the architecture of the network.
On the other hand, the accuracy of the gradient boosted decision tree provided
by the authors of the data set was approximately 97 \%. The reason was that the
variance of the data was too high, namely 881965160000000. The selected solution
to this was to standardize the data by calculating the standard score. Since the
resulting data contained NaN (Not a Number) values, the accuracy was still no
better than random. After setting the NaN values to zero, the neural networks
started learning.

The next step was to examine the data by performing a PCA (Principal Component
Analysis). The PCA revealed that the first dimension of the new space contains
approximately 3.5 \% of variance and that the first 1024 components contain
approximately 90 \% of variance. Since training with 1024 dimensional vectors is
much faster than training with the original vectors, and the loss of information
was acceptable, we decided to utilize this reduction.

% ---------------------------------------------

\section{Experiments}

The neural networks in the experiments are implemented by leveraging the Keras
\footnote{\url{https://keras.io/}} library. We have also employed a
hyper-parameter optimization technique called random search which is implemented
in the Keras Tuner \footnote{\url{https://keras-team.github.io/keras-tuner/}}
library.

Besides the accuracy metric for model evaluation, we will also consider the
precision and recall metrics. These metrics use the notion of true and false
positives and negatives. In malware detection, a false positive means that the
file is considered malicious even though it is actually harmless. On the other
hand, a false negative means that a malicious activity has not been detected.

In our case, precision describes the cost of false positives (when our model
falsely thinks that a benign file is malicious) and is defined by the following
formula:

\[
  Precision = \frac{\sum True Positive}{\sum True Positive + \sum False Positive}.
\]

Recall is defined in a similar fashion:

\[
  Recall = \frac{\sum True Positive}{\sum True Positive + \sum False Negative}.
\]

Note that the denominator in the definition of recall is in fact the sum of all
actually positive samples. Recall describes how good our model is at capturing
files that are truly malicious and as such, it can be seen as the probability of
detection.

We will also use the terms FPR (False Positive Rate) and TPR (True Positive
Rate). FPR is defined by the following formula:

\[
  FPR = \frac{\sum False Positive}{\sum True Negative + \sum False Negative}.
\]

Notice again that the denominator is the sum of all truly negative samples. TPR
is defined in the same way as recall and is therefore just a synonym. We will
utilize ROC (Receiver Operating Characteristic) curves that show the relation
between FPR and TPR. The goal for a binary classifier is to have high TPR at low
FPR. We can therefore compare our models based on this metric. Another metric is
the AUC (Are Under Curve). This metric integrates the area under the ROC curve.
An ideal classifier would have the AUC equal to 1.

\subsection{Multilayer Perceptron Networks}

Since machine learning utilizing neural networks is an empirical realm, we start
by experimenting with the network architecture and its hyper-parameters. In
order to estimate these parameters, the model was run multiple times on a
reduced data set for 5 epochs.

The MLP model was run for 6000 times by the random search routine with various
hyper-parameters and the best 10 runs had the following common properties:
dropout was set to zero, the activation function was ReLU (Rectified Linear
Unit) and the optimizing method was Adam (Adaptive Moment Estimation). The layer
width of the best performing models was mostly between 512 and 64 neurons.
Larger or smaller layers were not preferred. These results provided with an
elementary insight into suitable models and the preceding hyper-parameters were
set for all the subsequent models.

The next step was to estimate the most appropriate depth of the network. The
model was run again for 3000 times and the best performing network had 4 layers
with 512 neurons, 2 layers with 128 neurons and one layer with 64 neurons. This
network was then retrained on the full data set for 50 epochs as further
training did not provide any performance improvement.

The last layer had always one neuron with a sigmoid function as the activation
function. The loss function was the cross entropy measure. The learning rate was
chosen automatically by the Keras library.

\subsection{Convolutional Neural Networks}
As mentioned in the introduction, CNN have also been used to statically analyse
malware. We have tried to build a 1D CNN in a similar fashion as described in
the previous section. This time; however, the search for hyper-parameters was
much slower due to higher computational demands - the model was much slower to
train and the hyper-parameter search space was much larger. The model was run
for 3000 times and the best performing network had the following properties: the
were three convolutional layers with 16 kernels, the size of each kernel was 32.
There were also four dense MLP layers with sizes 512, 256, 128 and 64. The
stride parameter of the convolutional layers was set manually to two in order to
speed up the training. The rest of the hyper-parameters was kept the same as in
the MLP model.

Researchers have also tried to detect malicious software by transforming it to
images and then apply 2D CNN \cite{cnn1, cnn2}. We have also tried such an
approach by transforming the feature vectors to matrices (with 32 rows and
columns since the feature vectors have 1024 dimensions) and then we naively
tried to adapt and retrain a pre-built CNN called VGG16 \cite{vgg16}. The
motivation for this was to find out whether a network with an architecture that
works well for recognizing general images could have good performance in our
domain as well. Originally, the network had 1000 output neurons. We added one
more neuron on top of this layer in order to transform the network into a binary
classifier. The activation function of the neuron was a sigmoid function. The
optimizing method and loss function were also chosen the same as in the MLP
model. The convolutional networks were also trained on the full data set for 50
epochs.

\section{Results}

\begin{table*}[!htb]
  \caption{Metrics comparison}
  \label{tab:metrics}
  \centering
  \begin{tabular}{lccc}
    \hline
    & Accuracy [\%] & Precision [\%] & Recall [\%] \\
    \hline
    MLP                    & 95.04         & 95.52          & 94.51       \\
    GBDT                   & 93.61         & 92.40          & 95.05       \\
    CONV                   & 93.51         & 93.22          & 93.85       \\
    Adapted VGG16          & 93.13         & 93.94          & 92.22       \\
    MLP (no preprocessing) & 52.73         & 51.41          & 99.59       \\
    \hline
  \end{tabular}
\end{table*}

Table \ref{tab:metrics} shows the metrics comparison of trained models on the
test data. The MLP model has the highest accuracy, it also outperforms the
baseline GBDT provided by the ember library. It is also worth noting that the
MLP model has much plainer architecture than the VGG16 network for example.

\begin{figure}[!htb]
  \centering \includegraphics[width=\columnwidth]{roc}
  \caption{ROC comparison}
  \label{fig:roc}
\end{figure}

\begin{figure}[!htb]
  \centering \includegraphics[width=\columnwidth]{roc_zoom}
  \caption{ROC comparison (zoomed-in)}
  \label{fig:roc-zoom}
\end{figure}

Figures \ref{fig:roc} and \ref{fig:roc-zoom} show the ROC curves for our trained
models. The MLP model has the best ROC up to around 15 \% FPR where it is
surpassed by the GBDT model. The adapted VGG16 has actually the poorest
performance.

\begin{figure}[!htb]
  \centering \includegraphics[width=\columnwidth]{hist_mlp}
  \caption{MLP classification distribution}
  \label{fig:mlp-hist}
\end{figure}

Figure \ref{fig:mlp-hist} depicts the classification distribution of the MLP
model. We can see that for most samples, the model gives a confident correct
answer. However, there is also a considerable amount of samples for which the
model gives the wrong answer with certainty as well.

\begin{figure}[!htb]
  \centering \includegraphics[width=\columnwidth]{hist_conv}
  \caption{CONV classification distribution}
  \label{fig:conv-hist}
\end{figure}

\begin{figure}[!htb]
  \centering \includegraphics[width=\columnwidth]{hist_p_conv}
  \caption{Adapted VGG16 classification distribution}
  \label{fig:p-conv-hist}
\end{figure}

Figures \ref{fig:conv-hist} and \ref{fig:p-conv-hist} show that the
convolutional networks have similar behavior to the MLP model as regards the
classification distribution.

\begin{figure}[!htb]
  \centering \includegraphics[width=\columnwidth]{hist_mlp_raw}
  \caption{MLP (no preprocessing) classification distribution}
  \label{fig:raw-mlp-hist}
\end{figure}

Figure \ref{fig:raw-mlp-hist} shows the classification distribution for the MLP
model which classified the raw data from the data set. We can see that the model
classified most samples as positive (the output value for most samples was 0.5).
This is the reason why this model has the highest recall in table
\ref{tab:metrics}. However, the precision is very low and since the data set
contains equal amount of benign and malicious samples, the accuracy is close to
the accuracy of a random classifier which is exactly 50 \%.

\section{Dimensionality reduction}
Besides PCA, a VAE (Variational AutoEncoder) can also by used to reduce the
dimensionality of the data. We have experimentally built and trained a VAE that
reduces our data to three dimensions. The architecture and hyper-parameters of
the encoder and decoder of the VAE were chosen so that they match the best
performing MLP model.

\begin{figure}[!htb]
  \centering \includegraphics[width=\columnwidth]{vae-dim}
  \caption{Dimensionality reduction by a VAE}
  \label{fig:vae-dim}
\end{figure}

Image \ref{fig:vae-dim} shows the resulting three-dimensional data. We see that
all the samples are gathered in a single dense cloud which means that the
classification is not a straightforward task. For a comparison, image
\ref{fig:pca-dim} shows the same data after a reduction to three dimensions by
PCA.

\begin{figure}[!htb]
  \centering \includegraphics[width=\columnwidth]{pca-dim}
  \caption{Dimensionality reduction by PCA}
  \label{fig:pca-dim}
\end{figure}

% -------------------------------------------------------------------------------
% REFERENCE LIST
% -------------------------------------------------------------------------------

\bibliographystyle{plain} \bibliography{b}

% -------------------------------------------------------------------------------

\end{document}
