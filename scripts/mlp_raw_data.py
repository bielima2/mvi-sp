import numpy as np

from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras import metrics
from tensorflow.keras import optimizers

import helpers as h

BATCH_SIZE = 8192
EPOCHS = 50

try:
    x_train, y_train
except NameError:
    x_train = np.load('data/x_train_raw.npy')
    y_train = np.load('data/y_train.npy')
    x_val = np.load('data/x_val_raw.npy')
    y_val = np.load('data/y_val.npy')

m = keras.Sequential()
a = 'relu'

i = layers.Input(shape=(x_train.shape[1],))
a = 'relu'

o1 = layers.Dense(512, activation=a)(i)
o2 = layers.Dense(512, activation=a)(o1)
o3 = layers.Dense(512, activation=a)(o2)
o4 = layers.Dense(512, activation=a)(o3)
o5 = layers.Dense(128, activation=a)(o4)
o6 = layers.Dense(128, activation=a)(o5)
o7 = layers.Dense(64, activation=a)(o6)

p = layers.Dense(1, activation='sigmoid')(o7)

m = keras.Model(inputs=i, outputs=p)
m.compile(
    optimizer='adam',
    loss='binary_crossentropy',
    metrics=['accuracy', metrics.Precision(), metrics.Recall()])

m.fit(x_train, y_train,
      batch_size=BATCH_SIZE,
      epochs=EPOCHS,
      validation_data=(x_val, y_val))

m.save('out/mlp_raw.h5')
