import os
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn.preprocessing import normalize
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA

from tensorflow import keras
from tensorflow.keras.models import load_model
from tensorflow.keras import layers
from tensorflow.keras import models
from tensorflow.keras import metrics
from tensorflow.keras import optimizers
from tensorflow.keras import backend as K
from tensorflow.keras.losses import binary_crossentropy, mse
from tensorflow.keras.utils import plot_model

import helpers as h

BATCH_SIZE = 8192
EPOCHS = 10
A = 'relu'

try:
    x_train, y_train, x_test, y_test
except NameError:
    x_train = np.load('data/x_train_raw.npy')
    y_train = np.load('data/y_train.npy')
    x_test = np.load('data/x_test_raw.npy')
    y_test = np.load('data/y_test.npy')

    x_train = StandardScaler().fit_transform(x_train)
    x_train = np.nan_to_num(x_train)
    x_r = PCA(3).fit_transform(x_train)
    x_train = x_train / np.linalg.norm(x_train)

def sampling(args):
    z_mean, z_log_var = args
    batch = K.shape(z_mean)[0]
    dim = K.int_shape(z_mean)[1]
    epsilon = K.random_normal(shape=(batch, dim))
    return z_mean + K.exp(0.5 * z_log_var) * epsilon


def plot_results(models, data, batch_size=BATCH_SIZE, model_name="vae_mnist"):
    encoder, decoder = models
    x, y = data
    os.makedirs(model_name, exist_ok=True)

    filename = os.path.join('out', "vae_dim.png")
    z_mean, _, _ = encoder.predict(x, batch_size=batch_size)
    plt.figure(figsize=(8, 6))
    ax = plt.subplot(111, projection='3d')
    ax.scatter(z_mean[y>.5, 0], z_mean[y>.5, 1], z_mean[y>.5, 2],
                label='malicious', marker='x', alpha=.4,)
    ax.scatter(z_mean[y<.5, 0], z_mean[y<.5, 1], z_mean[y<.5, 2],
                label='benign', marker='o', alpha=.2,)
    plt.legend(loc='best')
    for label in ax.xaxis.get_ticklabels()[::2]:
        label.set_visible(False)
    for label in ax.yaxis.get_ticklabels()[::2]:
        label.set_visible(False)
    ax.tick_params(axis='z', pad=8)
    plt.title('Dimensionality reduction by a VAE')
    # ax.set_xlabel("z[0]")
    # ax.set_ylabel("z[1]")
    # ax.set_zlabel('z[2]')
    # ax.set_xticks([])
    # ax.set_yticks([])
    # ax.set_zticks([])
    plt.savefig(filename, dpi=1200,)
    plt.close('all')

    # filename = os.path.join('out', "pca.png")
    # plt.figure(figsize=(8, 6))
    # ax = plt.subplot(111, projection='3d')
    # ax.scatter(x_r[y>.5, 0], x_r[y>.5, 1], x_r[y>.5, 2],
    #             label='malicious', marker='x', alpha=.4,)
    # ax.scatter(x_r[y<.5, 0], x_r[y<.5, 1], x_r[y<.5, 2],
    #             label='benign', marker='o', alpha=.2,)
    # for label in ax.xaxis.get_ticklabels()[::2]:
    #     label.set_visible(False)
    # plt.legend(loc='best')
    # plt.title('Dimensionality reduction by PCA')
    # plt.savefig(filename, dpi=1200,)
    # plt.close('all')
    

# network parameters
input_shape = (x_train.shape[1], )
original_dim = x_train.shape[1]
latent_dim = 3

# VAE model = encoder + decoder
# build encoder model
inputs = layers.Input(shape=input_shape, name='encoder_input')
x = layers.Dense(512, activation=A)(inputs)
x3 = layers.Dense(256, activation=A)(x)
x4 = layers.Dense(256, activation=A)(x3)
x5 = layers.Dense(256, activation=A)(x4)
x6 = layers.Dense(128, activation=A)(x5)
z_mean = layers.Dense(latent_dim, name='z_mean')(x6)
z_log_var = layers.Dense(latent_dim, name='z_log_var')(x6)

# use reparameterization trick to push the sampling out as input
# note that "output_shape" isn't necessary with the TensorFlow backend
z = layers.Lambda(sampling, output_shape=(latent_dim,),
                  name='z')([z_mean, z_log_var])

# instantiate encoder model
encoder = models.Model(inputs, [z_mean, z_log_var, z], name='encoder')
encoder.summary()
plot_model(encoder, to_file='out/vae_mlp_encoder.png', show_shapes=True)

# build decoder model
latent_inputs = layers.Input(shape=(latent_dim,), name='z_sampling')
x = layers.Dense(512, activation=A)(latent_inputs)
x3 = layers.Dense(256, activation=A)(x)
x4 = layers.Dense(256, activation=A)(x3)
x5 = layers.Dense(256, activation=A)(x4)
x6 = layers.Dense(128, activation=A)(x5)
outputs = layers.Dense(original_dim, activation='sigmoid')(x6)

# instantiate decoder model
decoder = models.Model(latent_inputs, outputs, name='decoder')
decoder.summary()
plot_model(decoder, to_file='out/vae_mlp_decoder.png', show_shapes=True)

# instantiate VAE model
outputs = decoder(encoder(inputs)[2])
vae = models.Model(inputs, outputs, name='vae_mlp')

models = (encoder, decoder)
reconstruction_loss = binary_crossentropy(inputs, outputs)
reconstruction_loss *= original_dim
kl_loss = 1 + z_log_var - K.square(z_mean) - K.exp(z_log_var)
kl_loss = K.sum(kl_loss, axis=-1)
kl_loss *= -0.5
vae_loss = K.mean(reconstruction_loss + kl_loss)
vae.add_loss(vae_loss)
vae.compile(optimizer='adam')
vae.summary()
plot_model(vae, to_file='out/vae_mlp.png', show_shapes=True)

vae.fit(x_train, epochs=EPOCHS, batch_size=BATCH_SIZE,)

plot_results(models, (x_train, y_train), batch_size=BATCH_SIZE,
             model_name="vae_mlp")
