import pathlib
import ember
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import normalize
from sklearn.decomposition import PCA

DATA_PATH = 'ember/ember2018/'
DATA_OUT = 'data/'
VAL_SET_SIZE = 60000
COMP_NUM = 1024

pathlib.Path('data/').mkdir(parents=True, exist_ok=True) 
pathlib.Path('out/').mkdir(parents=True, exist_ok=True) 

x_train, y_train, x_test, y_test = ember.read_vectorized_features(DATA_PATH)

x_train = x_train[y_train >= 0]
y_train = y_train[y_train >= 0]

r = np.arange(len(x_train))
np.random.shuffle(r)
x_train = x_train[r]
y_train = y_train[r]

x_val = x_train[:VAL_SET_SIZE]
y_val = y_train[:VAL_SET_SIZE]

x_train = x_train[VAL_SET_SIZE:]
y_train = y_train[VAL_SET_SIZE:]

np.save('data/x_train_raw', x_train)
np.save('data/y_train', y_train)
np.save('data/x_val_raw', x_val)
np.save('data/y_val', y_val)
np.save('data/x_test_raw', x_test)
np.save('data/y_test', y_test)

s = StandardScaler()
x_train = s.fit_transform(x_train)
x_train = np.nan_to_num(x_train)
x_val = s.transform(x_val)
x_val = np.nan_to_num(x_val)
x_test = s.transform(x_test)
x_test = np.nan_to_num(x_test)

p = PCA()
p.fit(x_train)
v = p.explained_variance_ratio_

print('Variance of the 1st component: ', v[0])
print(f'Variance of the {COMP_NUM}th component:' , v[COMP_NUM-1])
print(f'Sum of variances of the first {COMP_NUM} components: ',
      np.sum(v[: COMP_NUM-1]))

p = PCA(n_components=COMP_NUM)
x_train = p.fit_transform(x_train)
x_val = p.transform(x_val)
x_test = p.transform(x_test)

np.save('data/x_train', x_train)
np.save('data/x_val', x_val)
np.save('data/x_test', x_test)

BATCH_SIZE = 8192
TRAIN_SET_SIZE_RED = BATCH_SIZE * 18
VAL_SET_SIZE_RED = BATCH_SIZE * 5
x_train_red = x_train[:TRAIN_SET_SIZE_RED]
y_train_red = y_train[:TRAIN_SET_SIZE_RED]
x_val_red = x_val[:VAL_SET_SIZE_RED]
y_val_red = y_val[:VAL_SET_SIZE_RED]

np.save('data/x_train_red', x_train_red)
np.save('data/y_train_red', y_train_red)
np.save('data/x_val_red', x_val_red)
np.save('data/y_val_red', y_val_red)
