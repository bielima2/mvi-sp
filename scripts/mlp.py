import numpy as np
import matplotlib.pyplot as plt

from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras import metrics
from tensorflow.keras import optimizers

import helpers as h

BATCH_SIZE = 8192
EPOCHS = 50

try:
    x_train, y_train, x_val, y_val, x_test, y_test
except NameError:
    (x_train, y_train, x_val, y_val, x_test, y_test, x_train_red,
     y_train_red, x_val_red, y_val_red) = h.load_data()

i = layers.Input(shape=(x_train.shape[1],))
a = 'relu'

o1 = layers.Dense(512, activation=a)(i)
o2 = layers.Dense(512, activation=a)(o1)
o3 = layers.Dense(512, activation=a)(o2)
o4 = layers.Dense(512, activation=a)(o3)
o5 = layers.Dense(128, activation=a)(o4)
o6 = layers.Dense(128, activation=a)(o5)
o7 = layers.Dense(64, activation=a)(o6)

p = layers.Dense(1, activation='sigmoid')(o7)

m = keras.Model(inputs=i, outputs=p)

m.compile(
    optimizer='adam',
    loss='binary_crossentropy',
    metrics=['accuracy', metrics.Precision(), metrics.Recall()])

h = m.fit(x_train, y_train,
          batch_size=BATCH_SIZE,
          epochs=EPOCHS,
          validation_data=(x_val_red, y_val_red))

m.save('out/mlp.h5')

plt.figure()
plt.plot(h.history['accuracy'])
plt.plot(h.history['val_accuracy'])
plt.plot(h.history['loss'])
plt.plot(h.history['val_loss'])
# plt.plot(h.history['precision_1'])
# plt.plot(h.history['val_precision_1'])
# plt.plot(h.history['recall_1'])
# plt.plot(h.history['val_recall_1'])
plt.title('Model accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig('out/mlp_training.svg')

plt.close('all')
