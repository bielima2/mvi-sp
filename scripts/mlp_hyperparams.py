import numpy as np

from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras import metrics
from tensorflow.keras import optimizers

from kerastuner import tuners
from kerastuner.engine.hypermodel import HyperModel
from kerastuner.engine.hyperparameters import HyperParameters

from tensorflow.keras.utils import plot_model

import helpers as h

OPTIMIZERS = ['sgd', 'rmsprop', 'adagrad', 'adam', 'adamax', 'nadam']
ACTIVATIONS = ['elu', 'selu', 'softplus', 'softsign', 'relu',
               'tanh', 'sigmoid', 'linear']
DROPOUT = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5]
BATCH_SIZE = 8192
EPOCHS = 4
MAX_TRIALS = 6000
OBJECTIVE = 'val_accuracy'


class MyHyperModel(HyperModel):
    def build(self, hp):
        m = keras.Sequential()
        a = hp.Choice('activation', ACTIVATIONS)
        d = hp.Choice('dropout', DROPOUT)
        for i in range(hp.Int('number of 1024 perceptron layers', 0, 5)):
            m.add(layers.Dense(units=1024, activation=a))
            m.add(layers.Dropout(d))

        for i in range(hp.Int('number of 512 perceptron layers', 0, 5)):
            m.add(layers.Dense(units=512, activation=a))
            m.add(layers.Dropout(d))

        for i in range(hp.Int('number of 256 perceptron layers', 0, 5)):
            m.add(layers.Dense(units=256, activation=a))
            m.add(layers.Dropout(d))

        for i in range(hp.Int('number of 128 perceptron layers', 0, 5)):
            m.add(layers.Dense(units=128, activation=a))
            m.add(layers.Dropout(d))

        for i in range(hp.Int('number of 64 perceptron layers', 0, 5)):
            m.add(layers.Dense(units=64, activation=a))
            m.add(layers.Dropout(d))

        m.add(layers.Dense(1, activation='sigmoid'))

        m.compile(
            optimizer=hp.Choice('optimizer', OPTIMIZERS),
            loss='binary_crossentropy',
            metrics=['accuracy', metrics.Precision(), metrics.Recall()])
        return m


if __name__ == '__main__':
    try:
        x_train, y_train, x_val, y_val, x_test, y_test
    except NameError:
        (x_train, y_train, x_val, y_val, x_test, y_test, x_train_red,
         y_train_red, x_val_red, y_val_red) = h.load_data()

    hypermodel = MyHyperModel()

    tuner = tuners.RandomSearch(
        hypermodel,
        objective=OBJECTIVE,
        max_trials=MAX_TRIALS,
        directory='../out/mlp_hyperparams',
        project_name ='mlp_hyperparams',
        overwrite=True)

    tuner.search_space_summary()

    tuner.search(x_train_red, y_train_red,
                 validation_data=(x_val_red, y_val_red),
                 epochs=EPOCHS,
                 batch_size=BATCH_SIZE)

    tuner.results_summary()
