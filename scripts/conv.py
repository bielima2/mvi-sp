import numpy as np
import matplotlib.pyplot as plt

from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras import metrics
from tensorflow.keras import optimizers
from tensorflow.keras.utils import plot_model

import helpers as h

BATCH_SIZE = 8192
EPOCHS = 50

m = keras.Sequential()
a = 'relu'
p = 'valid'
ker_size = 32
ker_num = 16

try:
    x_train, y_train, x_val, y_val, x_test, y_test
except NameError:
    (x_train, y_train, x_val, y_val, x_test, y_test, x_train_red,
     y_train_red, x_val_red, y_val_red) = h.load_data()
    x_train = np.expand_dims(x_train, -1)
    x_val = np.expand_dims(x_val, -1)
    x_test = np.expand_dims(x_test, -1)
    x_train_red = np.expand_dims(x_train_red, -1)
    x_val_red = np.expand_dims(x_val_red, -1)

m.add(layers.Conv1D(ker_num, ker_size, padding=p, activation=a, strides=2,
                    input_shape=(1024,1)))

m.add(layers.Conv1D(ker_num, ker_size, padding=p, activation=a, strides=2,))

m.add(layers.Conv1D(ker_num, ker_size, padding=p, activation=a, strides=2,))

m.add(layers.Flatten())

m.add(layers.Dense(units=512, activation=a))
m.add(layers.Dense(units=256, activation=a))
m.add(layers.Dense(units=128, activation=a))
m.add(layers.Dense(units=64, activation=a))

m.add(layers.Dense(1, activation='sigmoid'))

m.compile(
    optimizer='adam',
    loss='binary_crossentropy',
    metrics=['accuracy', metrics.Precision(), metrics.Recall()])


h = m.fit(x_train, y_train,
                validation_data=(x_val, y_val),
                epochs=EPOCHS,
                batch_size=BATCH_SIZE)

m.save('out/conv.h5')

plt.figure()
plt.plot(h.history['accuracy'])
plt.plot(h.history['val_accuracy'])
plt.plot(h.history['loss'])
plt.plot(h.history['val_loss'])
# plt.plot(h.history['precision_1'])
# plt.plot(h.history['val_precision_1'])
# plt.plot(h.history['recall_1'])
# plt.plot(h.history['val_recall_1'])
plt.title('Model accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.savefig('out/conv_training.svg')

plt.close('all')
