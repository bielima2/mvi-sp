import numpy as np

from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras import metrics
from tensorflow.keras import optimizers

from kerastuner import tuners
from kerastuner.engine.hypermodel import HyperModel
from kerastuner.engine.hyperparameters import HyperParameters

from tensorflow.keras.utils import plot_model
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input

import helpers as h

BATCH_SIZE = 4096
EPOCHS = 50
OBJECTIVE = 'val_accuracy'

if __name__ == '__main__':
    try:
        x_train, y_train, x_val, y_val, x_test, y_test
    except NameError:
        (x_train, y_train, x_val, y_val, x_test, y_test, x_train_red,
         y_train_red, x_val_red, y_val_red) = h.load_data()

        x_train = np.expand_dims(x_train, -1)
        x_val = np.expand_dims(x_val, -1)
        x_test = np.expand_dims(x_test, -1)
        x_train_red = np.expand_dims(x_train_red, -1)
        x_val_red = np.expand_dims(x_val_red, -1)

        a = np.reshape(x_train, (x_train.shape[0], 32, 32, 1))
        x_train = np.pad(a, ((0,0), (0,0), (0,0), (0,2)), constant_values=0)
        # x_train = preprocess_input(x_train)


    initial_m = VGG16(weights="imagenet", include_top=False,
                      input_tensor = layers.Input(shape=(32,32,3)))

    x = layers.Flatten()(initial_m.output)

    x = layers.Dense(1, activation='sigmoid')(x)

    m = keras.Model(initial_m.input, x)
    # for layer in initial_m.layers: layer.trainable=False

    m.compile(optimizer='adam',
              loss='binary_crossentropy',
              metrics=['accuracy', metrics.Precision(), metrics.Recall()])

    m.fit(x_train, y_train,
          batch_size=BATCH_SIZE,
          epochs=EPOCHS,)


    m.save('out/vgg16.h5')
