from tensorflow.keras.models import load_model
from sklearn.metrics import roc_curve
from sklearn.metrics import auc
import matplotlib.pyplot as plt
import os
import lightgbm as lgb
import numpy as np
from sklearn.metrics import accuracy_score, precision_score, recall_score

import helpers as h

try:
    x_train, y_train, x_val, y_val, x_test, y_test, y_pred_mlp, auc_lgbm
except NameError:
    (x_train, y_train, x_val, y_val, x_test, y_test, x_train_red,
     y_train_red, x_val_red, y_val_red) = h.load_data()
    x_test_raw = np.load('data/x_test_raw.npy')
    x_test_c = np.expand_dims(x_test, -1)
    a = np.reshape(x_test_c, (x_test_c.shape[0], 32, 32, 1))
    x_test_pc = np.pad(a, ((0,0), (0,0), (0,0), (0,2)), constant_values=0)

    mlp = load_model('out/mlp.h5')
    y_pred_mlp = mlp.predict(x_test).ravel()
    fpr_mlp, tpr_mlp, thresholds_mlp = roc_curve(y_test, y_pred_mlp)
    auc_mlp = auc(fpr_mlp, tpr_mlp)

    mlp_raw = load_model('out/mlp_raw.h5')
    y_pred_mlp_raw = mlp_raw.predict(x_test_raw).ravel()
    fpr_mlp_raw, tpr_mlp_raw, thresholds_mlp_raw = roc_curve(y_test,
                                                             y_pred_mlp_raw)
    auc_mlp_raw = auc(fpr_mlp_raw, tpr_mlp_raw)

    conv = load_model('out/conv.h5')
    y_pred_conv = conv.predict(x_test_c).ravel()
    fpr_conv, tpr_conv, thresholds_conv = roc_curve(y_test, y_pred_conv)
    auc_conv = auc(fpr_conv, tpr_conv)

    p_conv = load_model('out/vgg16.h5')
    y_pred_p_conv = p_conv.predict(x_test_pc).ravel()
    fpr_p_conv, tpr_p_conv, thresholds_p_conv = roc_curve(y_test, y_pred_p_conv)
    auc_p_conv = auc(fpr_p_conv, tpr_p_conv)

    lgbm = lgb.Booster(model_file='out/lgbm.txt')
    y_pred_lgbm = lgbm.predict(x_test_raw)
    fpr_lgbm, tpr_lgbm, thresholds_lgbm = roc_curve(y_test, y_pred_lgbm)
    auc_lgbm = auc(fpr_lgbm, tpr_lgbm)

# mlp.evaluate(x)

plt.figure()
plt.plot([0, 1], [0, 1], 'c--')
plt.plot(fpr_mlp, tpr_mlp, label='MLP (AUC = {:.3f})'.format(auc_mlp),
         color='b')
plt.plot(fpr_lgbm, tpr_lgbm, label='GBDT (AUC = {:.3f})'.format(auc_lgbm),
         color='g')
plt.plot(fpr_mlp_raw, tpr_mlp_raw, color='r',
         label='MLP, no preprocessing (AUC = {:.3f})'.format(auc_mlp_raw),)
plt.plot(fpr_conv, tpr_conv, color='y',
         label='CONV NET (AUC = {:.3f})'.format(auc_conv))
plt.plot(fpr_p_conv, tpr_p_conv, color='k',
         label='Adapted VGG16 (AUC = {:.3f})'.format(auc_p_conv))
plt.xlabel('False positive rate')
plt.ylabel('True positive rate')
plt.title('ROC curves')
plt.legend(loc='best')
plt.savefig('out/roc.pdf')

plt.figure()
plt.xlim(0, .4)
plt.ylim(.8, 1)
plt.plot(fpr_mlp[tpr_mlp>=.8], tpr_mlp[tpr_mlp>=.8], color='b',
         label='MLP (AUC = {:.3f})'.format(auc_mlp))
plt.plot(fpr_conv[tpr_conv>=.8], tpr_conv[tpr_conv>=.8], color='y',
         label='CONV NET (AUC = {:.3f})'.format(auc_conv))
plt.plot(fpr_lgbm[tpr_lgbm>=.8], tpr_lgbm[tpr_lgbm>=.8], color='g',
         label='GBDT (AUC = {:.3f})'.format(auc_lgbm))
plt.plot(fpr_p_conv[tpr_p_conv>=.8], tpr_p_conv[tpr_p_conv>=.8], color='k',
         label='Adapted VGG16 (AUC = {:.3f})'.format(auc_p_conv))
plt.xlabel('False positive rate')
plt.ylabel('True positive rate')
plt.title('ROC curves (zoomed in at top left)')
plt.legend(loc='best')
plt.savefig('out/roc_zoom.pdf')

plt.figure()
plt.hist(y_pred_mlp[y_test>.5], 10, alpha=.5, label='malicious')
plt.hist(y_pred_mlp[y_test<.5], 10, alpha=.5, label='benign')
plt.xlabel('Score')
plt.title('MLP recognition histogram')
plt.legend(loc='best')
plt.yscale('log')
plt.savefig('out/hist_mlp.pdf')

plt.figure()
plt.hist(y_pred_conv[y_test>.5], 10, alpha=.5, label='malicious')
plt.hist(y_pred_conv[y_test<.5], 10, alpha=.5, label='benign')
plt.xlabel('Score')
plt.title('CONV NET recognition histogram')
plt.legend(loc='best')
plt.yscale('log')
plt.savefig('out/hist_conv.pdf')

plt.figure()
plt.hist(y_pred_p_conv[y_test>.5], 10, alpha=.5, label='malicious')
plt.hist(y_pred_p_conv[y_test<.5], 10, alpha=.5, label='benign')
plt.xlabel('Score')
plt.title('Adapted VGG16 recognition histogram')
plt.legend(loc='best')
plt.yscale('log')
plt.savefig('out/hist_p_conv.pdf')

plt.figure()
plt.hist(y_pred_mlp_raw[y_test>.5], 10, alpha=.5, label='malicious')
plt.hist(y_pred_mlp_raw[y_test<.5], 10, alpha=.5, label='benign')
plt.xlabel('Score')
plt.title('MLP (no preprocessing) recognition histogram')
plt.legend(loc='best')
plt.yscale('log')
plt.savefig('out/hist_mlp_raw.pdf')

plt.close('all')

# mlp_eval = mlp.evaluate(x_test, y_test)
# conv_eval = conv.evaluate(x_test_c, y_test)
# p_conv_eval = p_conv.evaluate(x_test_pc, y_test)
# mlp_raw_eval = mlp_raw.evaluate(x_test_raw, y_test)

# print('MLP: loss, accuracy, precision, recall: ')
# print(mlp_eval)

# print('CONV: loss, accuracy, precision, recall: ')
# print(conv_eval)

# print('Pretrained CONV: loss, accuracy, precision, recall: ')
# print(p_conv_eval)

# print('MLP (no preprocessing): loss, accuracy, precision, recall: ')
# print(mlp_raw_eval)

print('MLP accuracy:  ', accuracy_score(y_test, y_pred_mlp.round()))
print('MLP precision: ', precision_score(y_test, y_pred_mlp.round()))
print('MLP recall:    ', recall_score(y_test, y_pred_mlp.round()))
print('')

print('CONV accuracy:  ', accuracy_score(y_test, y_pred_conv.round()))
print('CONV precision: ', precision_score(y_test, y_pred_conv.round()))
print('CONV recall:    ', recall_score(y_test, y_pred_conv.round()))
print('')

print('pretrained CONV accuracy:  ', accuracy_score(y_test,
                                                    y_pred_p_conv.round()))
print('pretrained CONV precision: ', precision_score(y_test,
                                                     y_pred_p_conv.round()))
print('pretrained CONV recall:    ', recall_score(y_test,
                                                  y_pred_p_conv.round()))
print('')

print('MLP (no preprocessing) accuracy:  ',
      accuracy_score(y_test, y_pred_mlp_raw.round()))
print('MLP (no preprocessing) precision: ',
      precision_score(y_test, y_pred_mlp_raw.round()))
print('MLP (no preprocessing) recall:    ',
      recall_score(y_test, y_pred_mlp_raw.round()))
print('')

print('LGBM accuracy:  ', accuracy_score(y_test, y_pred_lgbm.round()))
print('LGBM precision: ', precision_score(y_test, y_pred_lgbm.round()))
print('LGBM recall:    ', recall_score(y_test, y_pred_lgbm.round()))
print('')

