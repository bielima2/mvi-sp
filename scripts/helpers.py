import numpy as np

def load_data():
    return (np.load('data/x_train.npy'), np.load('data/y_train.npy'),
       np.load('data/x_val.npy'), np.load('data/y_val.npy'),
       np.load('data/x_test.npy'), np.load('data/y_test.npy'),
       np.load('data/x_train_red.npy'), np.load('data/y_train_red.npy'),
       np.load('data/x_val_red.npy'), np.load('data/y_val_red.npy'))
