import numpy as np

from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras import metrics
from tensorflow.keras import optimizers

from kerastuner import tuners
from kerastuner.engine.hypermodel import HyperModel
from kerastuner.engine.hyperparameters import HyperParameters

from tensorflow.keras.utils import plot_model

BATCH_SIZE = 8192
EPOCHS = 4
MAX_TRIALS = 2000
OBJECTIVE = 'val_accuracy'

def load_data():
    return np.load('x_train.npy'), np.load('y_train.npy'), np.load('x_val.npy'), \
        np.load('y_val.npy'), np.load('x_test.npy'), np.load('y_test.npy'), \
        np.load('x_train_red.npy'), np.load('y_train_red.npy'), \
        np.load('x_val_red.npy'), np.load('y_val_red.npy')


class MyHyperModel(HyperModel):
    def build(self, hp):
        a = 'relu'
        m = keras.Sequential()
        m.add(layers.Conv1D(16,
                            hp.Choice('kernel size',
                                      [4, 8, 16, 32, 64, 128, 256]),
                            padding='valid',
                            activation='relu',
                            strides=2,
                            input_shape=(1024,1)))

        for i in range(hp.Int('number of conv. layers', 0, 2)):
            m.add(layers.Conv1D(16,
                                hp.Choice('kernel size in layer ' + str(i+1),
                                          [4, 8, 16, 32, 64, 128]),
                                padding='valid',
                                activation='relu',
                                strides=2,))

        m.add(layers.Flatten())

        for i in range(hp.Int('number of 512 perceptron layers', 0, 3)):
            m.add(layers.Dense(units=512, activation=a))

        for i in range(hp.Int('number of 256 perceptron layers', 0, 3)):
            m.add(layers.Dense(units=256, activation=a))

        for i in range(hp.Int('number of 128 perceptron layers', 0, 3)):
            m.add(layers.Dense(units=128, activation=a))

        for i in range(hp.Int('number of 64 perceptron layers', 0, 3)):
            m.add(layers.Dense(units=64, activation=a))

        m.add(layers.Dense(1, activation='sigmoid'))

        m.compile(
            optimizer='adam',
            loss='binary_crossentropy',
            metrics=['accuracy', metrics.Precision(), metrics.Recall()])
        return m


if __name__ == '__main__':
    try:
        x_train, y_train, x_val, y_val, x_test, y_test
    except NameError:
        (x_train, y_train, x_val, y_val, x_test, y_test, x_train_red,
         y_train_red, x_val_red, y_val_red) = load_data()
        x_train = np.expand_dims(x_train, -1)
        x_val = np.expand_dims(x_val, -1)
        x_test = np.expand_dims(x_test, -1)
        x_train_red = np.expand_dims(x_train_red, -1)
        x_val_red = np.expand_dims(x_val_red, -1)

    hypermodel = MyHyperModel()

    tuner = tuners.RandomSearch(
        hypermodel,
        objective=OBJECTIVE,
        max_trials=MAX_TRIALS,
        overwrite=True)

    tuner.search_space_summary()

    tuner.search(x_train_red, y_train_red,
                 validation_data=(x_val_red, y_val_red),
                 epochs=EPOCHS,
                 batch_size=BATCH_SIZE)

    tuner.results_summary()
