# Malware detection utilizing ANN

See the `report.pdf` document for a thorough description.

## Install

Clone the repository and also clone the
[EMBER](https://github.com/endgameinc/ember) repository into a subdirectory.
Install the EMBER library by following
[README.md](https://github.com/endgameinc/ember#installation).

Download the [EMBER data
set](https://pubdata.endgame.com/ember/ember_dataset_2018_2.tar.bz2) from 2018
and place it into the `ember` subdirectory under the name `ember2018`.Then run
`python ember/scripts/train_ember.py ember/ember2018`. You should be good to go
by now. You might also need to install some other dependencies should you get
import errors.

## How to reproduce the experiments

All experiments are carried out by the python scripts in the `scripts`
directory. Beware of the memory and computational requirements - the scripts
usually eat up to 15GB of memory. A graphics card with at least 12GB of memory
comes in handy as well. The memory demands are also the reason why the code is
split up into smaller independent pieces which exchange data through the disk
memory.

**preprocessing.py** prepares the data set - it standardizes the data and
performs the PCA. It generates new files into the `data` directory. This script
must be run first.

**mlp_hyperparams.py** searches for the best hyper parameters and architecture
of the MLP model.

**mlp_best_depth.py** tries to determine the ideal depth of the MLP model. 

**mlp.py** trains the MLP model and stores it in the `out` directory.

**conv_hyperparams.py** searches for the best hyper parameters and architecture
of the network with convolutional layers.

**vgg16.py** adapts and retrains the pre-built model (VGG16) to fit the EMBER
data set.

**conv.py** trains the network with convolutional layers and stores the model in
the `out` directory.

__base_lgbm.py__ trains the baseline gradient boosted decision tree model
provided by the ember library. The model is stored in the `out` directory.

__mlp_raw_data.py__ trains the MLP model on raw data and stores it in the `out`
directory.

__vae.py__ trains a VAE in order to transform the data into three dimensions, it
also uses PCA for a comparison

__models_analysis.py__ evaluates all the previous models and produces figures in
the `out` directory. This script can be run only after the previous scripts have
been executed.
